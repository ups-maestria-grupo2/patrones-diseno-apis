/*
 Navicat Premium Data Transfer

 Source Server         : pglocal
 Source Server Type    : PostgreSQL
 Source Server Version : 140001
 Source Host           : localhost:5432
 Source Catalog        : patronesapi
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140001
 File Encoding         : 65001

 Date: 11/10/2022 21:11:52
*/


-- ----------------------------
-- Table structure for calidad_datos
-- ----------------------------
DROP TABLE IF EXISTS "public"."calidad_datos";
CREATE TABLE "public"."calidad_datos" (
  "id" int4 NOT NULL DEFAULT nextval('calidad_datos_id_seq'::regclass),
  "id_compania" varchar(255) COLLATE "pg_catalog"."default",
  "ranking" int4
)
;

-- ----------------------------
-- Table structure for datos
-- ----------------------------
DROP TABLE IF EXISTS "public"."datos";
CREATE TABLE "public"."datos" (
  "id" int4 NOT NULL DEFAULT nextval('datos_id_seq'::regclass),
  "id_compania" varchar(255) COLLATE "pg_catalog"."default",
  "cargo" varchar(255) COLLATE "pg_catalog"."default",
  "titulo" varchar(255) COLLATE "pg_catalog"."default",
  "area_estudios" varchar(255) COLLATE "pg_catalog"."default",
  "industria" varchar(255) COLLATE "pg_catalog"."default",
  "anios_experiencia" int4,
  "millas_metropolis" int4,
  "salario" numeric(10,2)
)
;

-- ----------------------------
-- Primary Key structure for table calidad_datos
-- ----------------------------
ALTER TABLE "public"."calidad_datos" ADD CONSTRAINT "calidad_datos_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table datos
-- ----------------------------
CREATE INDEX "ix_datos" ON "public"."datos" USING btree (
  "id_compania" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "cargo" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "titulo" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "area_estudios" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "industria" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table datos
-- ----------------------------
ALTER TABLE "public"."datos" ADD CONSTRAINT "datos_pkey" PRIMARY KEY ("id");
