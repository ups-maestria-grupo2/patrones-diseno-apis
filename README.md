# Patrones de diseño de APIs 

## Proyecto final: Prototipo de APIs Web
## Universidad Poliltecnica Salesiana - Maestria de Software

Integrantes:

- Daniel Burbano
- José Castillo
- Klever Simaliza
- Carlos Párraga
- Manuel Gómez

## Descripción

Este proyecto es una aplicación web que expone un conjunto de APIs para consulta de datos simulando
un servicio sobre información de profesionales.

## Formato de datos

Todos los apis de consumo de informaciòn retornan la siguiente estructura en formato JSON:

```
{
  data : null,
  error : null,
  parametros : {}
}
```

Donde data es el resultado de la consulta si existen datos o null por defecto.
El campo error tiene un mensaje si es que presenta un error.
Parametros contiene un arreglo de clave valor de lo que se envió a la consulta si existe

## Endpoints disponibles

Las palabras entre llaves denotan un valor que se tiene que pasar al api como argumento

- GET: /api/parametros: Devuelve una lista de los posibles parámetros que se pueden consultar 
- GET: /api/valores/{nombre_parametro}: Recibe {nombre_parametro} y devuelte en data una lista con los valores únicos del parámetro escogido 
- GET: /api/cuenta/{parametro}/{valor}: Recibe {parametro} y un {valor} y retorna la cuenta de cuantos registros existen con ese filtro, p.ej /cuenta/titulo/DOCTORAL
- GET: /api/salarioPromedio/{parametro}/{valor}: Recibe {parametro} y un {valor} y retorna el promedio de salario del filtro seleccionado, p.ej /cuenta/titulo/DOCTORAL
- GET: /api/busqueda/{parametro}/{valor}: Recibe {parametro} y un {valor} y retorna los primeros 10000 registros que cumplan el criterio ordenados por años de experiencia del personal

## Requerimientos para instalación y despliegue

- PHP 8.0 o superior
- Composer para gestión de paquetes
- Servidor PostgreSQL v14 o superior (base de datos).

