<?php

namespace Tests;

use App\Models\DatosCargo;
use App\Models\ServicioConsultas;
use Illuminate\Support\Facades\DB;

class BaseTest extends TestCase {

    function test_que_se_conecte() {
        $cuenta = DB::table('datos')->count();
        $this->assertGreaterThan(0, $cuenta, 'No hay datos');
    }

    function test_primerdato() {
        /** @var DatosCargo $rec */
        $rec = DatosCargo::query()->first();
        echo $rec->id;
        $this->assertNotEmpty($rec);
        $this->assertNotEmpty($rec->id);
    }

    function test_api1() {
        $s = new ServicioConsultas();
        $lista = $s->listaCargos();
        print_r($lista);
    }
}
