<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function() use ($router) {
    //return $router->app->version();
    return 'Patrones de Diseño de APIs v1.0';
});

$router->group(['prefix' => '/api'], function() use ($router) {
    $router->get('/parametros', 'ConsultasController@listaParametros');
    $router->get('/valores/{parametro}', 'ConsultasController@listaValores');
    $router->get('/cuenta/{parametro}/{valor}', 'ConsultasController@cuentaParametros');
    $router->get('/salarioPromedio/{parametro}/{valor}', 'ConsultasController@salarioPromedio');
    $router->get('/busqueda/{parametro}/{valor}', 'ConsultasController@busqueda');
});

