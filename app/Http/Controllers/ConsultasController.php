<?php

namespace App\Http\Controllers;

use App\Models\RespuestaConsulta;
use App\Models\ServicioConsultas;

class ConsultasController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    function listaParametros() {
        $s = new ServicioConsultas();
        $res = $s->listaParametros();
        return $this->retornarRespuesta($res);
    }

    function listaValores($parametro) {
        $s = new ServicioConsultas();
        $res = $s->listaValores($parametro);
        return $this->retornarRespuesta($res);
    }

    function cuentaParametros($parametro, $valor) {
        $s = new ServicioConsultas();
        $res = $s->cuentaPorParametro($parametro, $valor);
        return $this->retornarRespuesta($res);
    }

    function salarioPromedio($parametro, $valor) {
        $s = new ServicioConsultas();
        $res = $s->salarioPromedio($parametro, $valor);
        return $this->retornarRespuesta($res);
    }

    function busqueda($parametro, $valor) {
        $s = new ServicioConsultas();
        $res = $s->busqueda($parametro, $valor);
        return $this->retornarRespuesta($res);
    }

    protected function retornarRespuesta(RespuestaConsulta $res) {
        $r = response()->json($res);
        if (!empty($res->error))
            $r->setStatusCode(400);
        return $r;
    }

}
