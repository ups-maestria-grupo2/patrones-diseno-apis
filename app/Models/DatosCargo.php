<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string id
 * @property string id_compania
 * @property string tipo_cargo
 * @property string titulo
 * @property string area_estudios
 * @property string industria
 * @property string anios_experiencia
 * @property string millas_metropolis
 * @property string salario
 */
class DatosCargo extends Model {
	protected $table = 'datos';
	public $timestamps = false;
	protected $guarded = [];

}
