<?php

namespace App\Models;

class RespuestaConsulta {
    var $data;
    var $parametros = [];
    var $error;

    /**
     * @param $error
     * @param $data
     */
    public function __construct($data = null, $error = null) {
        $this->error = $error;
        $this->data = $data;
    }

    static function conError($error) {
        return new RespuestaConsulta(null, $error);
    }
}
