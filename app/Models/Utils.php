<?php

namespace App\Models;

class Utils {

	static function dinero($v) {
		if ($v === null or $v === '')
			return null;
		return floatval(
			number_format($v, 2, '.', '')
		);
	}
}
