<?php

namespace App\Models;

use Hamcrest\Util;
use Illuminate\Support\Facades\DB;

class ServicioConsultas {

    static $listas = [
        'cargo',
        'titulo',
        'industria',
        'area_estudios'
    ];

    function busqueda($campo, $valor) {
        if (!in_array($campo, self::$listas)) {
            return RespuestaConsulta::conError('Parametro de busqueda no reconocido');
        }

        $q = DB::table('datos as d')
            ->selectRaw('id_compania,
            cargo,
            titulo,
            area_estudios,
            industria,
            anios_experiencia,
            millas_metropolis,
            salario')
            ->orderByRaw('anios_experiencia desc')
            ->limit(10000);
        $q->where($campo, $valor);
        return new RespuestaConsulta(
            $q->get()->toArray()
        );
    }

    function listaDistintaCampo($campo) {
        return DB::table('datos')
            ->selectRaw($campo)
            ->groupBy($campo)
            ->orderBy($campo)->get()
            ->pluck($campo)
            ->toArray();
    }

    function listaParametros($par) {
        if (!in_array($par, self::$listas)) {
            return RespuestaConsulta::conError('Parametro de busqueda no reconocido');
        }
        return new RespuestaConsulta(
            self::$listas
        );
    }

    function listaValores($par) {
        if (!in_array($par, self::$listas)) {
            return RespuestaConsulta::conError('Parametro de busqueda no reconocido');
        }
        return new RespuestaConsulta(
            $this->listaDistintaCampo($par)
        );
    }

    function cuentaPorParametro($campo, $valor) {
        if (!in_array($campo, self::$listas)) {
            return RespuestaConsulta::conError('Parametro de busqueda no reconocido');
        }
        $cuenta = DB::table('datos')
            ->where($campo, $valor)
            ->count();
        $res = new RespuestaConsulta($cuenta);
        $res->parametros = [
            $campo => $valor
        ];
        return $res;
    }

    function salarioPromedio($campo, $valor) {
        if (!in_array($campo, self::$listas)) {
            return RespuestaConsulta::conError('Parametro de busqueda no reconocido');
        }
        $prom = DatosCargo::query()
            ->where($campo, $valor)
            ->selectRaw('avg(salario) as prom')
            ->first()
            ->prom;

        $res = new RespuestaConsulta(
            Utils::dinero($prom)
        );
        $res->parametros = [
            $campo => $valor
        ];
        return $res;
    }

}
